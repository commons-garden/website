---
title: The Paradox of Empowerment
display: public
status: ongoing
description: How to empower communities while expanding their autonomy
facilitator: [nonlinear]
date: 2024-05-08
---

## Facilitation Links:

- [Shared notes](https://hackmd.io/@commonsgarden/empowerment)
- [Signal group](https://signal.group/#CjQKIFMl9PqjcicG1VRxjGyUg3E0aZywtkmK8EMjgIrvfSJ9EhAMp_n8LUohi913hlQypIUf)

## Related links:

- [Power Awareness Tool 2.0](https://www.partos.nl/publicatie/power-awareness-tool-2-0/)