---
title: SSB collections
description: A way for peers to curate posts
scope: [SSB]

status: draft
display: public
license: pending

author: [nonlinear, staltz]
reviewed: [nonlinear, staltz]

date: 2023-06-30
---

Reframing SSB tags as collections

[View hackmd](https://hackmd.io/@praxis/ssb-collections)

> {.aaa} Resurfacing posts outside chronological order, reducing Bias of Now

> Think "mixmix's book of soup recipes" "nonlinear book of illustration tips" or "rabble book of East Asia travel spots"

> Some users don't write much, but curate existing content. Collections give them a way to shine.

> Tags were poorly named. Under the hood they behave like a collection, but they don't have order.

> Book collections can have internal chronological order. Or author can reorder if they want. It's on them.

> Difference from SSB tags:

    - how is presented
    - books are part of profile (for discoverability)
    - books are authoral, and people can follow books for changes

> SSB is long form, slow reading, we should honor old posts.

> Posts should have CC licensing, so Books can have licensing too.

> disruptors:
>
> - edit
> - initial order: chronological (per post or per joining book)
> - groups
> - licensing
