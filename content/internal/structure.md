---
title: Structure
display: draft
description: Category JSON match
date: 2024-02-19
---

Categories have properties

## Specs

- description
- type:
  - freeform: facilitator can write anything
  - hybrid-list: can be chosen by item, or freeform
  - list: needs to be chosen by item
- items: specifies the elements that can be chosen if type is hybrid-list or list
- required: frontmatter criteria needs to be written by facilitator (true or false)
