---
title: choosing display
display: public
description: How to present categories/posts
date: 2023-11-28
---

## draft

Use it for articles that are nto ready to be published

## public

Public articles appear on lists and index

## private

Private articles never appear on lists or index
