---
title: Āhau
display: public
description: free community archiving / family tree tool made in New Zealand
date: 2023-07-18
members: [mixmix, ben_tairea, cherese_eiripa]
contacts: [mixmix, ben_tairea]
scopes: [SSB, Hyper]
link: https://ahau.io
needs: [design, maori translation]
---
